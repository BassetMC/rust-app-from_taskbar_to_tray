use systray::Application;
use winapi::um::winuser::{ShowWindow, SHOW_OPENWINDOW ,SW_HIDE, GetParent};
use winapi::um::wincon::GetConsoleWindow;

fn main() {
    let mut app = Application::new().unwrap();
    // app.set_icon_from_file("Path/To/Your/Icon.ico").unwrap();
    app.add_menu_item("Show", |_| {
        println!("Showing application");
        // Add your code to show the application here
        unsafe {
            let window = GetConsoleWindow(); // cmd
            let wnd = GetParent(window.clone()); // win 11 terminal
            if wnd.is_null() { 
                ShowWindow(window, SHOW_OPENWINDOW);
            }
            else{
                if window.is_null() { 
                    println!("Failed to get console window");
                } else {
                    ShowWindow(wnd, SHOW_OPENWINDOW);
                }
            }
        }
        Ok::<_, systray::Error>(())
    }).unwrap();
    app.add_menu_item("Hide", |_| {
        println!("Hiding application");
        // Add your code to hide the application here
        unsafe {
            let window = GetConsoleWindow(); // cmd
            let wnd = GetParent(window.clone()); // win 11 terminal
            if wnd.is_null() { 
                ShowWindow(window, SW_HIDE);
            }
            else{
                if window.is_null() { 
                    println!("Failed to get console window");
                } else {
                    ShowWindow(wnd, SW_HIDE);
                }
            }
        }
        Ok::<_, systray::Error>(())
    }).unwrap();
    app.wait_for_message().unwrap();
}